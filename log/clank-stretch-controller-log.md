## 2021 12 03 

Z SPU is... 16t pinion -> 84t drive, 20T drive... 

200 steps / rev * 16 microsteps for 3200  

* 84/16 for 5.25 * 16800 steps / rev of drive  

20T * 2mm, 40mm travel / rev ... 16800 / 40 = 420 SPU, nice  

## 2021 12 06 

Machine is finally together & moving well etc... now for better controller APIs. 

I think actually I won't make any big changes, can do that later with good motion systems.

I do need to figure the full size of this thing... I'll jog around to find it. There should be a probing routine for this, check that out... 

Configured those, now I have a home & bounding box (since it homes to top right, and 0,0,0 is bottom left). 

Wrote a little thing that checks to make sure we don't break any microstepping laws... based on tick rates, per-axis maximum speeds, and steps-per-unit. 

```js
  // now we can do this little reporting:
  // motion systems have a base rate for issuing steps (or read encoder pulses), 
  // so we want to check that we are not breaking any microsecond laws 
  let maxTickRate = 10000
  for(let mot in this.motors){
    let tickRate = this.motion.config.maxRate[mot.charAt(0)] * this.motors[mot].config.SPU 
    if(tickRate > maxTickRate) {
      console.warn(`motor ${mot} config exceeds max tick rate, ${tickRate} > ${maxTickRate}`)
      let ntr = Math.floor((maxTickRate / tickRate) * this.motion.config.maxRate[mot.charAt(0)])
      this.motion.config.maxRate[mot.charAt(0)] = ntr 
      console.warn('new max rate for this axis is', ntr)
    }
  }
```

Now I want to try a tool-put-down routine... the demo that I want is just to swap pen plotters on a job, so I'll be back at gcode generation w/ tool numbers, tool state (eventually)... etc 

OK I think that in practice, driving the servo w/ the h-bridge doesn't make sense... I was hopeful that this would mean no config, but we still have to toggle direction. Also, I want to *know* always whether it's closed or open... *and* it's like, jamming itself into some stuck positions, so I will re-roll that controller / vm for a servo-type control. 

Servo control is totally better, that was a red herring project. Servo is literally red, should've known. 

Now I want... the tool UI structure... so that I can

- on machine state setup, have user tell machine which tool is loaded 
- click on new-state-we-want, operate 
- tool-config-machine: pickup and putdown routines... 
  - should just have to config place & whether up-then-retract or just pullout 
- print more-chill pen plotter, and pp & 2 ? 

### Ideal Config System

- on hello, should be able to run a routine:
  - pokes around for limits, just by stalling motors on hard stops... open loop motor control w/ encoder for stall detect 
  - uses these bounds to tune motors each in closed loop 
  - just needs to know which axis are tied to which... 
  - saves system to config file 