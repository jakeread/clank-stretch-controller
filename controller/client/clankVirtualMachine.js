/*
clankVirtualMachine.js

vm for Clank-FXY / all things physically attached (end effector not included)

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2021

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import { PK, TS, VT, EP, TIMES } from '../osapjs/core/ts.js'
import MotionVM from '../osapjs/vms/motionVirtualMachine.js'
import MotorVM from '../osapjs/vms/motorVirtualMachine.js'
import BladePlateVM from '../osapjs/vms/bladePlateVirtualMachine.js'

export default function ClankVM(osap) {

  // ------------------------------------------------------ NET LOCATION OF BUS HEAD 

  let headRoute = PK.route().sib(0).pfwd().sib(1).pfwd().end()

  // position after-homing:
  // since we home to top-right, bottom-left is 0,0,0
  // and we have bounding box then... 
  let posOnHome = {
    X: 620,     // about 0->130mm x should be safe,
    Y: 430,     // about 0->170mm y should be safe
    Z: 37       // 260mm tall max, abt 
  }

  // ------------------------------------------------------ MOTION
  // with base route -> embedded smoothie instance 
  this.motion = new MotionVM(osap, headRoute)

  // .settings() for rates and accels, 
  this.motion.settings({
    accel: {  // mm/sec^2 
      X: 500,   // 1500
      Y: 500,   // 1500
      Z: 250,   // 300, 
      E: 150    // 500 
    },
    maxRate: {  // mm/sec 
      X: 400,   // 100
      Y: 400,   // 100 
      Z: 100,    // 50 
      E: 50    // 100 
    },
    spu: {
      X: 20,
      Y: 20,
      Z: 105,
      E: 200
    }
  })

  // ------------------------------------------------------ MOTORS

  // clank fxy:
  // AXIS   SPU     INVERT    BUS
  // X:     320     false     1
  // YL:    320     true      2
  // YR:    320     false     3
  // Z:     x       x         4 

  this.motors = {
    X: new MotorVM(osap, PK.route(headRoute).sib(1).bfwd(1).end()),
    YL: new MotorVM(osap, PK.route(headRoute).sib(1).bfwd(2).end()),
    YR: new MotorVM(osap, PK.route(headRoute).sib(1).bfwd(3).end()),
    Z: new MotorVM(osap, PK.route(headRoute).sib(1).bfwd(4).end())
  }

  // .settings() just preps for the .init() or whatever other call, 
  this.motors.X.settings({
    axisPick: 0,
    axisInversion: false,
    microstep: 4,
    currentScale: 0.3,
    homeRate: 300, // units / sec
    homeOffset: 100, // units 
  })

  this.motors.YL.settings({
    axisPick: 1,
    axisInversion: true,
    microstep: 4,
    currentScale: 0.3,
    homeRate: -300,
    homeOffset: 100,
  })

  this.motors.YR.settings({
    axisPick: 1,
    axisInversion: false,
    microstep: 4,
    currentScale: 0.3,
    homeRate: 300,
    homeOffset: 100
  })

  this.motors.Z.settings({
    axisPick: 2,
    axisInversion: true,
    microstep: 4,
    currentScale: 0.4,
    homeRate: -250,
    homeOffset: 100
  })

  // ------------------------------------------------------ setup / handle motor group

  this.setupMotors = async () => {
    for (let mot in this.motors) {
      try {
        await this.motors[mot].setup()
      } catch (err) {
        console.error(`failed to setup ${mot}`)
        throw err
      }
    }
  }

  this.enableMotors = async () => {
    for (let mot in this.motors) {
      try {
        await this.motors[mot].enable()
      } catch (err) {
        console.error(`failed to enable ${mot}`)
        throw err
      }
    }
  }

  this.disableMotors = async () => {
    for (let mot in this.motors) {
      try {
        await this.motors[mot].disable()
      } catch (err) {
        console.error(`failed to disable ${mot}`)
        throw err
      }
    }
  }

  // ------------------------------------------------------ HOMING 

  this.homeZ = async () => {
    try {
      await this.motion.awaitMotionEnd()
      if (this.motors.Z) {
        await this.motors.Z.home()
        await this.motors.Z.awaitHomeComplete()
      } else {
        console.warn("on clank.homeZ, no z motor... passing...")
      }
    } catch (err) { throw err }
  }

  this.homeXY = async () => {
    try {
      await this.motion.awaitMotionEnd()
      if (this.motors.X) await this.motors.X.home()
      if (this.motors.YL) await this.motors.YL.home()
      if (this.motors.YR) await this.motors.YR.home()
      if (this.motors.X) await this.motors.X.awaitHomeComplete()
      if (this.motors.YL) await this.motors.YL.awaitHomeComplete()
      if (this.motors.YR) await this.motors.YR.awaitHomeComplete()
    } catch (err) { throw err }
  }

  this.home = async () => {
    try {
      await this.homeZ()
      await this.homeXY()
      await this.motion.setPos(posOnHome)
    } catch (err) { throw err }
  }

  // ------------------------------------------------------ TOOL CHANGING

  this.toolChanger = new BladePlateVM(osap, PK.route(headRoute).sib(1).bfwd(5).end())
  this.toolChanger.setMicroSet(1100, 1850)

  // ------------------------------------------------------ TOOLS 

  this.tools = {}
  this.currentTool = { name: 'unknown' }

  this.addTool = (toolName, postPosition) => {
    this.tools[toolName] = {
      index: Object.keys(this.tools).length,
      name: toolName,
      postPosition: postPosition,
    }
  }

  this.dropCurrentTool = async () => {
    // ... resolve when dropped 
  }

  let pickupRate = 80
  let approachAngle = 35
  let delY = 18
  let delX = Math.tan(approachAngle * (Math.PI/180)) * delY 
  console.warn('delx, y', delX, delY) 

  this.getTool = async (toolRequest) => {
    if (!this.tools[toolRequest] && toolRequest != 'no tool') {
      throw new Error(`bad tool name, given ${toolRequest}`)
    }
    try {
      // do we currently know what tool we have?
      if (this.currentTool.name == 'unknown') throw new Error(`need to know current tool before picking next`)
      // if we have a current tool... 
      if (this.currentTool.name != 'no tool') {
        // put it down, 
        let baseMove = { 
          rate: 500, 
          position: {
            X: this.currentTool.postPosition.X,
            Y: this.currentTool.postPosition.Y,
            Z: this.currentTool.postPosition.Z
          }
        }
        // pull up just in front,
        baseMove.position.Y += 100 
        await this.motion.goTo(baseMove)
        baseMove.rate = pickupRate * 0.5
        baseMove.position.Y -= 50
        baseMove.rate = pickupRate * 0.15
        baseMove.position.Y -= 50
        await this.motion.goTo(baseMove)
        await this.toolChanger.setLeverState(false)
        baseMove.position.X -= delX 
        baseMove.position.Y += delY 
        baseMove.rate = pickupRate * 0.05 
        await this.motion.goTo(baseMove)
        baseMove.position.Y += 100
        baseMove.rate = pickupRate 
        await this.motion.goTo(baseMove)
      }
      // if we want a new tool, 
      if (toolRequest != 'no tool') {
        await this.toolChanger.setLeverState(false)
        let baseMove = { 
          rate: 500, 
          position: {
            X: this.tools[toolRequest].postPosition.X,
            Y: this.tools[toolRequest].postPosition.Y,
            Z: this.tools[toolRequest].postPosition.Z
          }
        }
        // git pull tool 
        // rock up behind it, and left some... 
        baseMove.position.X -= delX 
        baseMove.position.Y += 100
        await this.motion.goTo(baseMove)
        // slow approach... 
        baseMove.rate = pickupRate
        baseMove.position.Y -= 100 - delY 
        await this.motion.goTo(baseMove) 
        // now make the face, 
        baseMove.position.X += delX 
        baseMove.position.Y -= delY 
        baseMove.rate = pickupRate * 0.05
        await this.motion.goTo(baseMove)
        // close the TC 
        await this.toolChanger.setLeverState(true)
        // now we do the lift, 
        // extraaact 
        baseMove.position.Y += 45
        baseMove.rate = pickupRate * 0.25
        await this.motion.goTo(baseMove)
        baseMove.position.Y += 55
        baseMove.rate = pickupRate
        await this.motion.goTo(baseMove)
        // now we have this tool 
        this.currentTool = this.tools[toolRequest]
      } else {
        this.currentTool = { name: 'no tool' }
      }
    } catch (err) { throw err }
  }

  /*

  // tool localization for put-down & pickup, tool statefulness, 
  // from back left 0,0 
  // put-down HE at (23.8, -177) -> (23.8, -222.6) -> release -> (-17.8, -208.6) clear -> (-17.8, -183)
  // { position: {X: num, Y: num, Z: num}, rate: num }

  this.goto = async (pos, rate) => {
    try {
      if (!rate) rate = 6000
      // set remote queue-wait-time 
      await this.setWaitTime(1)
      await delay(5)
      // wait for the stop 
      await this.awaitMotionEnd()
      await this.addMoveToQueue({
        position: { X: pos[0], Y: pos[1], Z: pos[2], E: 0 },
        rate: rate
      })
      await delay(5)
      await this.awaitMotionEnd()
      await this.setWaitTime(100)
    } catch (err) {
      console.error('during goto')
      throw err
    }
  }

  let tools = [{
    pickX: 16.8,
    pickY: -177,
    plunge: -45.6
  }]

  this.dropTool = async (num) => {
    try {
      await this.awaitMotionEnd()
      await this.closeTC()
      let cp = await this.getPos()
      await this.goto([tools[num].pickX, tools[num].pickY, cp.Z])
      console.warn('done setup')
      await this.delta([0, tools[num].plunge, 0])
      await this.openTC()
      await delay(250)
      console.warn('tc open')
      await this.delta([-6, 10, 0])
      await this.delta([0, 50, 0])
      await this.goto([tools[num].pickX, tools[num].pickY, cp.Z])
    } catch (err) {
      console.error(`at T${num} drop`)
      throw err
    }
  }

  this.pickTool = async (num) => {
    try {
      await this.awaitMotionEnd()
      await this.openTC()
      let cp = await this.getPos()
      await this.goto([tools[num].pickX, tools[num].pickY, cp.Z])
      await this.delta([-6, 0, 0])
      await this.delta([0, tools[num].plunge + 10, 0])
      await this.delta([6, -10, 0])
      await this.closeTC()
      await delay(250)
      await this.delta([0, -tools[num].plunge, 0])
      await delay(250)
    } catch (err) {
      console.error(`at T${num} pick`)
      throw err
    }
  }
  */
} // end clank vm 